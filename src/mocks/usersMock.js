export default () => ({
  users: [
    {
      id: 1,
      name: 'John',
      surname: 'Doe',
      occupation: 'CTO'
    },
    {
      id: 2,
      name: 'Sven',
      surname: 'Nllson',
      occupation: 'Developer'
    }
  ]
});
