import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import { withRouter } from 'react-router-dom';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(theme => ({
  root: {},
  menuButton: {
    marginRight: theme.spacing(2)
  }
}));

const AppToolbar = props => {
  const openUsersList = () => () => {
    props.history.push('/');
  };
  console.log(props);
  const classes = useStyles();
  const routName = props.location.state ? props.location.state.name : 'Users';
  const location = props.location.pathname;
  return (
    <AppBar position='relative'>
      <Toolbar className={classes.root}>
        {location === '/' || location === '/todo-list' ? (
          <Typography variant='h6' noWrap>
            {routName}
          </Typography>
        ) : (
          <IconButton
            edge='start'
            className={classes.menuButton}
            color='inherit'
            aria-label='menu'
            onClick={openUsersList()}
          >
            <ArrowBackIcon />
          </IconButton>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default withRouter(AppToolbar);
