import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import MenuDrawer from './MenuDrawer';
import AppToolbar from './AppToolbar';

import UserList from '../views/UserList/UserList';
import TodoList from '../views/TodoList/TodoList';
import UserDetail from '../views/UserDetail/UserDeatil';

import { Route, Switch } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  gridContainer: {
    height: '100vh'
  },
  drawerContaner: {
    position: 'relative'
  },
  contentContainer: {
    background: '#e6fff9',
    height: '100%'
  }
}));

function MainLayout() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Grid container className={classes.gridContainer}>
        <Grid item xs={2} className={classes.drawerContaner}>
          <MenuDrawer />
        </Grid>
        <Grid item xs={10} className={classes.contentContainer}>
          <AppToolbar />
          <Container>
            <Switch>
              <Route exact path='/' component={UserList} />
              <Route path='/todo-list' component={TodoList} />
              <Route path='/user-detail/:id' component={UserDetail} />
            </Switch>
          </Container>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default MainLayout;
