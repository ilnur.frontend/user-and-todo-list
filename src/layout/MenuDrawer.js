import React, { useState, useEffect } from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import GroupIcon from '@material-ui/icons/Group';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: '100%'
  },
  drawerPaper: {
    width: '100%',
    position: 'absolute'
  }
}));

const menuItems = [
  {
    name: 'Users',
    icon: <GroupIcon />,
    location: {
      pathname: '/',
      state: { name: 'Users' }
    }
  },
  {
    name: 'Todos',
    icon: <DoneAllIcon />,
    location: {
      pathname: '/todo-list',
      state: { name: 'Todos' }
    }
  }
];

const DrawerMenuList = props => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  function handleMenuItemClick(event, index) {
    setSelectedIndex(index);
  }

  const { location } = props;
  useEffect(() => {
    if (!location.includes('user') && location !== '/') {
      setSelectedIndex(1);
    }
  }, [location]);
  return (
    <List>
      {menuItems.map((item, index) => (
        <MenuItem
          button
          key={index}
          to={item.location}
          component={Link}
          selected={index === selectedIndex}
          onClick={event => handleMenuItemClick(event, index)}
        >
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.name} />
        </MenuItem>
      ))}
    </List>
  );
};

const MenuDrawer = props => {
  const classes = useStyles();
  const location = props.location.pathname;

  return (
    <Drawer
      className={classes.drawer}
      variant='persistent'
      classes={{
        paper: classes.drawerPaper
      }}
      anchor='left'
      open={true}
    >
      <div className={classes.toolbar} />
      <Divider />
      <DrawerMenuList location={location} />
    </Drawer>
  );
};

export default withRouter(MenuDrawer);
