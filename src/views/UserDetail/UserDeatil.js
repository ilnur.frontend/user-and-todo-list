import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import usersMock from './../../mocks/usersMock';
import { useParams } from 'react-router-dom';

const { users } = usersMock();

const useStyles = makeStyles(theme => ({
  root: {
    width: '50%',
    maxWidth: 450,
    backgroundColor: theme.palette.background.paper
  }
}));

const UserDetail = props => {
  let { id } = useParams();

  let user = users.find(user => user.id === Number(id));

  console.log(user);
  const classes = useStyles();
  return (
    <Fragment>
      <h1>User detail page {id}</h1>
    </Fragment>
  );
};

export default UserDetail;
