import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import { withRouter, Link } from 'react-router-dom';

const UserListItem = props => {
  const { name, surname, occupation, id } = props;
  const openUserDetail = id => () => {
    props.history.push(`/user-detail/${id}`);
  };
  return (
    <ListItem component={Link} button to={`/user-detail/${id}`}>
      <ListItemAvatar>
        <Avatar>
          <ImageIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText primary={name + ' ' + surname} secondary={occupation} />
    </ListItem>
  );
};

export default withRouter(UserListItem);
