import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import UserListItem from './components/UserListItem';
import usersMock from './../../mocks/usersMock';

const { users } = usersMock();

const useStyles = makeStyles(theme => ({
  root: {
    width: '50%',
    maxWidth: 450,
    backgroundColor: theme.palette.background.paper
  }
}));

const usersListItems = users.map((item, index) => (
  <UserListItem
    name={item.name}
    surname={item.surname}
    occupation={item.occupation}
    key={index}
    id={item.id}
  />
));

const UserList = () => {
  const classes = useStyles();
  return (
    <Fragment>
      <List className={classes.root}>{usersListItems}</List>
    </Fragment>
  );
};

export default UserList;
